package DAO;

import java.sql.Connection;
import java.sql.DriverManager;

public class Conexion {

    private static String db = "equipo6";
    private static String user = "kz";
    private static String pass = "kzroot";
    private static String ruta = "jdbc:mysql://usam-sql.sv.cds:3306/" + db;

    private Connection conec;

    public Connection getConec() {
        return conec;
    }

    public void setConec(Connection conec) {
        this.conec = conec;
    }

    public void conectarDB() throws Exception {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            this.conec = DriverManager.getConnection(ruta, user, pass);
            if (this.conec != null) {
                System.out.println("Conectado a DB Equipo 6");
            } else {
                System.out.println("Error al conectar");
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void desconectarDB() throws Exception {
        try {
            if (this.conec != null) {
                if (this.conec.isClosed() != true) {
                    this.conec.close();
                }
            }
        } catch (Exception e) {
            throw e;
        }
    }

}
